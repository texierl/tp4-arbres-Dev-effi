import java.util.Objects;

public class ABR{
    private int val;
    private ABR filsG;
    private ABR filsD;
    //fisG == null <=> filsD == null

    //on supposera en prérequis sous-entendu que this est un ABR dans toutes les méthodes (sauf mentionné explicitement,
    //pour  la méthode verifie)
    ///////////////////////////////////////////////////////
    ////// méthodes fournies
    ///////////////////////////////////////////////////////

    public ABR(){
        filsG = null;
        filsD=  null;
    }

    public ABR(int x, ABR g, ABR d){
        val = x;
        filsG = g;
        filsD=  d;
    }

    public ABR(Liste l){
        val=l.getVal();
        filsD = new ABR();
        filsG = new ABR();
        for (int i=1; i<l.longueur(); i++){
            this.insert(l.get(i));
        }
    }


    public int getVal(){
        //sur arbre non vide
        if(estVide())
            throw new RuntimeException("getVal appelée sur ABR vide");
        return val;
    }


    public boolean estVide(){
        return filsG==null;
    }

    public boolean egal(ABR a){
        if (estVide()&&a.estVide()) return true;
        if ((estVide()&&!a.estVide())||(!estVide()&&a.estVide())) return false;
        if (val==a.val) return filsD.egal(a.getFilsD()) && filsG.egal(a.getFilsG());
        return false;

    }


    public String toStringV2aux(String s){
        //pre : aucun
        //resultat : retourne une chaine de caracteres permettant d'afficher this dans un terminal (avec l'indentation du dessin precedent, et en ajoutant s au debut de chaque ligne ecrite) et passe a la ligne

        if( estVide ())
            return s+"()\n";
        else
            return filsD.toStringV2aux (s + "     ") + s + getVal() + "\n"+ filsG.toStringV2aux (s + "     ");
    }

    public String toString(){
        return toStringV2aux("");
    }





    ///////////////////////////////////////////////////////
    ////// méthodes demandées dans le TP
    ///////////////////////////////////////////////////////

    public boolean recherche(int x){
        if ((x<val && filsG.estVide())||(x>val && filsD.estVide()))return false;
        if (x == val)return true;
        if (x>val) return filsD.recherche(x);
        return filsG.recherche(x);
    }
    public void insert(int x){
        if (estVide()){
            val = x;
            filsG = new ABR();
            filsD = new ABR();
        }else if(x>=val){
            filsD.insert(x);
        }else {
            filsG.insert(x);
        }
    }

    public int max(){
        if (estVide())return Integer.MIN_VALUE;
        return Integer.max(val, filsD.max());
    }

    public int min(){
        if (estVide())return Integer.MAX_VALUE;
        return Integer.min(val, filsD.max());
    }

    public Liste toListeTriee(){
        if (estVide()) return new Liste();
        Liste l = new Liste();
        l.ajoutTete(val);
        l.concat(filsD.toListeTriee());
        Liste l2 = filsG.toListeTriee();
        l2.concat(l);
        return l2;
    }


    public void suppr(int x){
        if (!(x>val && filsD.estVide())||!(x<val && filsG.estVide())){
            if (val == x){
                if (!filsG.estVide()) {
                    val = filsG.max();
                    filsG.suppr(filsG.max());
                }else {
                    if (!filsD.estVide()) {
                        val = 0;
                    }else {
                        val = filsD.val;
                        filsD = filsD.filsD;
                        filsG = filsG.filsG;
                    }
                }
            }else if (x>=val){
                filsD.suppr(x);
            }else {
                filsG.suppr(x);
            }
        }
    }

    public Liste toListeTrieeV2Aux(Liste l){
        if (estVide()) return l;
        filsD.toListeTrieeV2Aux(l);
        l.ajoutTete(val);
        return filsG.toListeTrieeV2Aux(l);
    }

    public Liste toListeTrieeV2(){
        return toListeTrieeV2Aux(new Liste());
    }



    public boolean verifieNaive(){
        if (estVide()) return true;
        System.out.println("filsG.verifieNaive()"+filsG.verifieNaive());
        System.out.println("filsD.verifieNaive()"+filsD.verifieNaive());
        System.out.println("val>= filsG.max() && val<= filsD.min()"+(val>= filsG.max() && val<= filsD.min()));
        return filsG.verifieNaive()&&(val >= filsG.max() && val <= filsD.min())&&filsD.verifieNaive();
    }
    public boolean verifieV1(){
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR

        //appel à verifABRV1
        throw new RuntimeException("méthode non implémentée");
    }

    public boolean verifABRV1(int m, int M){
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR
        throw new RuntimeException("méthode non implémentée");
    }

    public boolean verifieV2(){
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR

        //appel à verifABRV2
        throw new RuntimeException("méthode non implémentée");
    }

    public int[] verifABRV2(){
        //prérequis : this est bien un arbre (au sens de la classe Arbre : soit les deux fils null, soit les deux fils non null)
        //mais pas forcément un ABR

        throw new RuntimeException("méthode non implémentée");
    }




    ///////////////////////////////////////////////////////
    ////// méthodes utiles seulement pour les tests
    ///////////////////////////////////////////////////////



    public ABR getFilsG() {
        return filsG;
    }

    public ABR getFilsD() {
        return filsD;
    }

    public void setVal(int val) {
        if(estVide())
            throw new RuntimeException("setVal appelée sur ABR vide");
        this.val = val;
    }

    public void setFilsG(ABR filsG) {
        this.filsG = filsG;
    }

    public void setFilsD(ABR filsD) {
        this.filsD = filsD;
    }

    public static void main(String[] args){
        ABR a = Test.construitArbre1();
        System.out.println(a);
        System.out.println(a.toListeTriee().toString());
        System.out.println(a.toListeTrieeV2().toString());

    }

}
